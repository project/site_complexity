<?php

namespace Drupal\site_complexity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Controller for Site Complexity.
 */
class SiteComplexityController extends ControllerBase {

  /**
   * Scan the site for elements contributing to complexity.
   */
  public function complexity() {

    $scoring = site_complexity_scoring();
    $components = [];
    $total = 0;

    $this->getComponentInfo($components, $scoring, $total);

    return $this->buildReport($components, $total);

  }

  /**
   * Generate report HTML.
   */
  private function buildReport($components, $total) {

    $previous_score = \Drupal::state()->get('site_complexity.previous_score');
    $previous_timestamp = \Drupal::state()->get('site_complexity.previous_timestamp');
    if ($previous_score == NULL) {
      $previous_score = 0;
    }

    // Build display.
    $markup = '';
    $markup .= '<div class="site_complexity">';
    $markup .= "<h3>Total Site Complexity Score: $total</h3>";
    $markup .= '<table class="site-complexity">';
    $markup .= '<caption>Score by Component:</caption>';
    $markup .= '<tr><th>Component</th>
    <th class="right--site-complexity">Points</th>
    <th class="right--site-complexity">Count</th>
    <th class="right--site-complexity">Score</th>
    </tr>';
    foreach ($components as $component) {
      $markup .= '<tr><td>' . $component[0] . '</td>
      <td class="right--site-complexity numeric--site-complexity">' . $component[1] . '</td>
      <td class="right--site-complexity numeric--site-complexity">' . $component[2] . '</td>
      <td class="right--site-complexity numeric--site-complexity">' . $component[3] . '</td>
      </tr>';
    }
    $markup .= "<tr><th>Total Complexity Score</th>
    <th></th>
    <th></th>
    <th class='right--site-complexity numeric--site-complexity'>$total</th>
    </tr>";
    $markup .= '</table>';
    $markup .= '</div>';

    $route = 'site_complexity.settings';
    $url = Url::fromRoute($route);
    $link = Link::fromTextAndUrl(t('Change scoring'), $url)->toString();
    $markup .= "<h3>$link</h3>";

    $content = [];
    $content['message'] = [
      '#markup' => $markup,
    ];

    return $content;
  }

  /**
   * Get component info.
   */
  private function getComponentInfo(&$components, $scoring, &$total) {
    $existing = \Drupal::service('extension.list.module')->reset()->getList();
    $enabled = system_get_info('module');
    $components[] = $this->addComponent('extensions', $existing, $scoring, $total);

    $components[] = $this->addComponent('enabled_extensions', $enabled, $scoring, $total);

    $contrib = $this->getContrib($enabled);
    $components[] = $this->addComponent('contrib_extensions', $contrib, $scoring, $total);

    $custom = $this->getCustom($enabled);
    $components[] = $this->addComponent('custom_extensions', $custom, $scoring, $total);

    $complex = $this->getComplex($enabled);
    $components[] = $this->addComponent('complex_extensions', $complex, $scoring, $total);

    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $components[] = $this->addComponent('content_types', $contentTypes, $scoring, $total);

    $contentTypes = \Drupal::service('entity.manager')->getStorage('media_type')->loadMultiple();
    $components[] = $this->addComponent('media_types', $contentTypes, $scoring, $total);

    $fields = \Drupal::entityManager()->getFieldMap();
    $listed_fields = [];
    foreach ($fields as $entity => $field_list) {
      foreach ($field_list as $key => $value) {
        $listed_fields[] = $key;
      }
    }
    $unique_fields = array_unique($listed_fields);
    $components[] = $this->addComponent('fields', $unique_fields, $scoring, $total);

    $vocabs = taxonomy_vocabulary_get_names();
    $components[] = $this->addComponent('vocabularies', $vocabs, $scoring, $total);

    $views = \Drupal::service('entity.manager')->getStorage('view')->loadMultiple();
    $components[] = $this->addComponent('views', $views, $scoring, $total);

    $blocks = \Drupal::service('entity.manager')->getStorage('block')->loadMultiple();
    $components[] = $this->addComponent('blocks', $blocks, $scoring, $total);

    $roles = $this->getRoles();
    $components[] = $this->addComponent('roles', $roles, $scoring, $total);

    $permissions = $this->getPermissions();
    $components[] = $this->addComponent('permissions', $permissions, $scoring, $total);

    /*
    // Libraries.
    $libraries = ??;
    $components[] = $this->addComponent('libraries', [], $scoring, $total);
     */
  }

  /**
   * Helper function to organize component data.
   */
  private function addComponent($label, $array, $scoring, &$total) {
    $count = count($array);
    $points = $scoring[$label]['points'];
    $score = $count * $points;
    $total += $score;
    return [$scoring[$label]['label'], $points, $count, $score];
  }

  /**
   * Complex extensions.
   */
  private function getComplex($enabled) {
    $config = \Drupal::configFactory()->getEditable('site_complexity.settings');
    $complex_modules = $config->get('complex_modules');

    $complex_enabled = [];
    foreach ($enabled as $key => $extension) {
      if (in_array($key, $complex_modules)) {
        $complex_enabled[] = $key;
      }
    }
    return $complex_enabled;
  }

  /**
   * Contrib extensions.
   */
  private function getContrib($enabled) {
    $contrib = [];
    foreach ($enabled as $key => $extension) {
      $path = drupal_get_path('module', $key);
      if (strpos($path, 'core') !== 0) {
        if (strpos($path, 'modules/custom') === 0) {
          // We count custom modules separately.
          continue;
        }
        $contrib[] = $key;
      }
    }
    return $contrib;
  }

  /**
   * Custom extensions.
   */
  private function getCustom($enabled) {
    $custom = [];
    foreach ($enabled as $key => $extension) {
      $path = drupal_get_path('module', $key);
      if (strpos($path, 'modules/custom') === 0) {
        $custom[] = $key;
      }
    }
    return $custom;
  }

  /**
   * Permissions.
   */
  private function getRoles() {
    $roles = array_keys(\Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple());

    return $roles;
  }

  /**
   * Permissions.
   */
  private function getPermissions() {
    $all_permissions = array_keys(\Drupal::service('user.permissions')->getPermissions());

    return $all_permissions;
  }

}
