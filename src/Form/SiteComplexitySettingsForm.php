<?php

namespace Drupal\site_complexity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure site_complexity settings for this site.
 */
class SiteComplexitySettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_complexity_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('site_complexity.settings');
    $scoring = site_complexity_scoring();
    foreach ($scoring as $key => $value) {
      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $value['label'],
        '#default_value' => $config->get('points.' . $key),
        '#size' => 3,
        '#maxlength' => 3,
      ];
    }

    $complex_modules = implode("\n", $config->get('complex_modules'));
    $form['complex_modules'] = [
      '#type' => 'textarea',
      '#title' => 'Complex modules',
      '#description' => 'List complex modules machine names, one per line.',
      '#default_value' => $complex_modules,
      '#rows' => 10,
      '#cols' => 20,
      '#resizable' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save updates'),
      '#button_type' => 'primary',
      '#submit' => ['::saveSubmit'],
    ];

    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to default'),
      '#button_type' => 'primary',
      '#submit' => ['::resetSubmit'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $scoring = site_complexity_scoring();
    foreach (array_keys($scoring) as $key) {
      $value = $form_state->getValue($key);
      if (!ctype_digit($value)) {
        $form_state->setErrorByName('integer points', $this->t('Point values must be integers.'));
      }
    }

    $complex_modules = $form_state->getValue('complex_modules');
    $complex_modules = str_replace("\r\n", "\n", $complex_modules);
    $complex_modules = str_replace("\r", "\n", $complex_modules);
    $complex_modules = explode("\n", $complex_modules);
    foreach ($complex_modules as $complex_module) {
      if (trim($complex_module == '')) {
        continue;
      }
      if (preg_match('{^[a-z_]+$}', trim($complex_module)) === 0) {
        $form_state->setErrorByName('valid module names',
          $this->t('Module names must consist of lower case letters and underscores.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function saveSubmit(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = \Drupal::configFactory()->getEditable('site_complexity.settings');

    $scoring = site_complexity_scoring();

    foreach (array_keys($scoring) as $key) {
      $config->set('points.' . $key, $form_state->getValue($key));
    }

    $new_complex_modules = [];
    $complex_modules = $form_state->getValue('complex_modules');
    $complex_modules = str_replace("\r\n", "\n", $complex_modules);
    $complex_modules = str_replace("\r", "\n", $complex_modules);
    $complex_modules = explode("\n", $complex_modules);
    foreach ($complex_modules as $complex_module) {
      if (trim($complex_module == '')) {
        continue;
      }
      $new_complex_modules[] = $complex_module;
    }
    $config->set('complex_modules', $new_complex_modules);

    $config->save();

    drupal_set_message($this->t('Point values have been updated.'));

    $form_state->setRedirect('site_complexity.complexity');

  }

  /**
   * {@inheritdoc}
   */
  public function resetSubmit(array &$form, FormStateInterface $form_state) {

    site_complexity_defaults();

    drupal_set_message($this->t('Point values have been reset to default.'));

    $form_state->setRedirect('site_complexity.complexity');

  }

}
